$(function() {

    var App = (function(){

        return {
            init : function() {
                DummyModule.init();
                FixHeight.init();
                MobileBehaviour.init();
                Popups.init();
                IE10.init();

                FormDogovor.init();
                FormInvite.init();
                FormGifts.init();
            }
        }
    })()

    ,FixHeight = (function(){
        var $sidebar = $(".sidebar"),
            $content = $(".content");

        return {

            fitHeight : function() {

                var h1 = $sidebar.height(),
                    h2 = $content.height(),
                    h3 = $(window).height() - 70;

                var W = $(window).width();
                if ( isMobile.any || (W < 900) ) {
                    $sidebar.css('height', 'auto');
                    $content.css('height', 'auto');
                    return false;
                }

                var H = Math.max(h1, h2);
                if ( h3 > H ) H = h3;
                $sidebar.css('height', H);
                $content.css('height', H);

                return false;
            },

            init : function() {
                if ( !isMobile.any ) {
                    $('body').addClass('no-mobile');
                }
                window.setTimeout(this.fitHeight, 400); // на всякий случай, если не успело
                $(window).on('resize', this.fitHeight); 
            }
        }
    })()

    ,MobileBehaviour = (function(){
        return {
            init : function() {
                if ( !$(".header__caller").length ) return false;
                $(".header__caller").on('click', function(){
                    $(this).toggleClass('header__caller--close');
                    $('.header__nav').toggleClass('header__nav--opened');
                    $('.header').toggleClass('header--nav-opened');
                    $('.header__tricolor').toggleClass('header__tricolor--showed');
                    $('body').toggleClass('no-scroll');
                    return false;
                })

                if ( !$(".sidebar__menu-caller").length ) return false;
                $(".sidebar__menu-caller").on('click', function(){
                    $(this).toggleClass('sidebar__menu-caller--close');
                    $('.sidebar__overlay-wrapper').toggleClass('sidebar__overlay-wrapper--opened');
                    $(".mobile-overlay").toggleClass('mobile-overlay--active');
                    $('body').toggleClass('no-scroll');
                    return false;
                });

                $(".sidebar__overlay-wrapper-close").on('click', function() {
                    $(".sidebar__menu-caller").toggleClass('sidebar__menu-caller--close');
                    $('.sidebar__overlay-wrapper').toggleClass('sidebar__overlay-wrapper--opened');
                    $(".mobile-overlay").toggleClass('mobile-overlay--active');
                    $('body').removeClass('no-scroll');
                    return false; 
                });

            }
        }
    })()

    ,Popups = (function(){
        var $popup_dogovor = $(".popup-dogovor");
        var $callers = $('.popup__caller');
        var $popups = $(".popup");

        return {

            popupsClose : function() {
                var redirect = $('.popup.active').attr('data-redirect') == 'true';
                $popups.removeClass('active');
                $('body').removeClass('no-scroll');

                if ( redirect ) {
                    if ( $("#extra_question").length ) {
                        document.location.href = $("#extra_question").attr('href');
                    }
                }
            },

            init : function() {
                var self = this;

                $(document).on('click', '.popup', function(ev){
                    if ( !$(ev.target).parents('.popup').length ) {
                        self.popupsClose();
                    }
                });

                $(document).on('click', '.popup__close', function(){
                    self.popupsClose();
                    return false;
                });

                $(document).keyup(function(e) {
                    if (e.keyCode == 27) self.popupsClose();
                });

                $callers.on('click', function(){
                    var $popup = $popups.filter('.popup-' + $(this).attr('data-popup') );
                    $popup.toggleClass('active');
                    $popup.attr('data-redirect', $(this).attr('data-redirect'));
                    $popup.find('input').first().focus();
                    $('body').toggleClass('no-scroll');

                    return false;
                });
            }
        }
    })()

    ,FormDogovor = (function(){
        return {
            init : function() {
                var $form = $("#form_dogovor");
                if ( !$form.length ) return false;

                if ( $('.input-phone').length ) $('.input-phone').mask("+7 (999) 999-99-99");

                $form.validate({
                    messages: {
                        eqiupment_id      : "Пожалуйста, укажите ID приёмного оборудования",
                        fio               : "Пожалуйста, укажите ФИО",
                        personal_mobile   : "Пожалуйста, укажите Ваш номер телефона",
                        address           : "Пожалуйста, укажите адрес доставки приза",
                        confirm_agreement : "Пожалуйста, поставьте галочку",
                    },
                    submitHandler: function(form) {
                        // сюда процесс сабмита вставляем
                        // $(form).ajaxSubmit();
                        $.ajax({
                            url : $(form).attr('action'),
                            method : $(form).attr('method'),
                            data : $(form).serialize(),
                            success : function(data) {
                                if ( data.error ) {
                                    $(form).next().find('.popup-dogovor__ihaveno-link').hide();
                                    $(form).next().find('.popup-dogovor__confirmation')
                                                  .text(data.errorMessage).show();
                                } else {
                                    $(form).find('.btn').removeClass('btn--active').addClass('btn--inactive').attr('disabled', true);
                                    $(form).next().find('.popup-dogovor__ihaveno-link').hide();
                                    $(form).next().find('.popup-dogovor__confirmation').text('Данные отправлены успешно').show();
                                
                                    if (data.userScore) {
                                        $('[data-player-score]').text(data.userScore);
                                    }
                                }
                            },
                            error : function(data) {
                                $(form).next().find('.popup-dogovor__ihaveno-link').hide();
                                $(form).next().find('.popup-dogovor__confirmation')
                                              .text(data.errorMessage).show();
                            }
                        });

                        return false;
                    }
                });

            }
        }
    })()

    ,FormInvite = (function(){
        return {
            init : function() {
                var $form = $("#form_invite");
                if ( !$form.length ) return false;

                $form.validate({
                    messages: {
                        'invite-email' : {
                            "required" : "Пожалуйста, укажите эл. адрес своей подруги",
                            "email" : "Пожалуйста, введите корректный эл. адрес"
                        }
                    },
                    submitHandler: function(form) {
                        // сюда процесс сабмита вставляем
                        // $(form).ajaxSubmit();
                        $.ajax({
                            url : $(form).attr('action'),
                            method : $(form).attr('method'),
                            data : $(form).serialize(),
                            success : function(data) {
                                if ( data.error ) {
                                    $(form).find('.popup__confirmation')
                                                  .text(data.errorMessage).show();
                                } else {
                                    $(form).find('.btn').removeClass('btn--active').addClass('btn--inactive').attr('disabled', true);
                                    $(form).find('.popup__confirmation').text('Приглашение отправлено').show();
                                    $(form).parents('.popup-invite')
                                            .removeClass('popup-invite--screen1')
                                            .addClass('popup-invite--screen2');

                                    if (data.userScore) {
                                        $('[data-player-score]').text(data.userScore);
                                    }
                                }
                            },
                            error : function(data) {
                                $(form).find('.popup__confirmation')
                                              .text(data.errorMessage).show();
                            }
                        });

                        return false;
                    }
                });

            }
        }
    })()

    ,FormGifts = (function(){
        return {
            init : function() {
                var $form = $("#form_gifts");
                if ( !$form.length ) return false;

                $("select").simpleselect({
                    displayContainerInside : 'document',
                    containerMargin : 0
                }).each(function(){
                    var n = $(this).find('option').length;
                    $(this).parent().next().find('.options').addClass('n-'+n);
                });

                $form.validate({
                    messages: {
                        'g-email'  : {
                            "required" : "Пожалуйста, укажите эл. адрес получателя",
                            "email" : "Пожалуйста, введите корректный эл. адрес"
                        },
                        'g-serial' : "Пожалуйста, выберите свой любимый сериал",
                        'g-name'   : "Пожалуйста, укажите ваше имя",
                        'g-gift'   : "Пожалуйста, укажите какой подарок хотите получить"
                    },
                    submitHandler: function(form) {
                        // сюда процесс сабмита вставляем
                        // $(form).ajaxSubmit();
                        $.ajax({
                            url : $(form).attr('action'),
                            method : $(form).attr('method'),
                            data : $(form).serialize(),
                            success : function(data) {
                                if ( data.error ) {
                                    $(form).find('.popup__confirmation')
                                                  .text(data.errorMessage).show();
                                } else {
                                    $(form).find('.btn').removeClass('btn--active').addClass('btn--inactive').attr('disabled', true);
                                    $(form).find('.popup__confirmation').text('Письмо с подсказкой отправлено').show();
                                
                                    if (data.userScore) {
                                        $('[data-player-score]').text(data.userScore);
                                    }
                                }
                            },
                            error : function(data) {
                                $(form).next().find('.popup__confirmation')
                                              .text(data.errorMessage).show();
                            }
                        });

                        return false;
                    }
                });

            }
        }
    })()

    ,IE10 = (function(){
        return {
            init : function() {

                function getIEVersion(){
                    var agent = navigator.userAgent;
                    var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
                    var matches = agent.match(reg);
                    if (matches != null) {
                        return { major: matches[1], minor: matches[2] };
                    }
                    return { major: "-1", minor: "-1" };
                }

                var ie_version =  getIEVersion();
                var is_ie10 = ie_version.major == 10;
                
                if (is_ie10) {
                    $("html").addClass("ie10");
                }
            }
        }
    })()

    /**
     * Dummy Module Example
     */
    ,DummyModule = (function(){
        return {
            init : function() {
                // do something
            }
        }
    })()

    ;App.init();

});
